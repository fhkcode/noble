/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/


#ifndef _ADC_H__
#define _ADC_H__
#include "BA45F5350.h"

//The macro definition cannot be changed
#define	AN0    		0x00
#define	AN1    		0x01
#define	AN2    		0x02
#define	AN3   		0x03
#define VBGREF		0x04
#define	OPA0O		0x05
#define	OPA1O		0x06
#define	LINEV		0x07

#define	ADC_8BIT	0	
#define	ADC_12BIT	1

#define	_RD_ADC_SET_AN0() 		{ _sadc1 = 0b00000011;_sadc0 = 0b00010000; }
#define	_RD_ADC_SET_AN1() 		{ _sadc1 = 0b00001011;_sadc0 = 0b00010001; }
#define	_RD_ADC_SET_AN2() 		{ _sadc1 = 0b00001010;_sadc0 = 0b00000010; }
#define	_RD_ADC_SET_AN3() 		{ _sadc1 = 0b00000011;_sadc0 = 0b00010011; }
#define	_RD_ADC_SET_VBGREF() 	{ _sadc1 = 0b00101011;_sadc0 = 0b00010111; }
#define	_RD_ADC_SET_OPA0O()	 	{ _sadc1 = 0b01000011;_sadc0 = 0b00010111; }
#define	_RD_ADC_SET_OPA1O()	 	{ _sadc1 = 0b01101011;_sadc0 = 0b00010111; }
#define	_RD_ADC_SET_LINEV()	 	{ _sadc1 = 0b10000011;_sadc0 = 0b00010111; }


#define		_ADC_ON()		{_adcen=1;}
#define		_ADC_OFF()		{_adcen=0;}	

void ADC_Init();
unsigned int  Read_ADC_Channel(unsigned char CH,unsigned char ADC_BIT);

#endif
