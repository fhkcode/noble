/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "board.h"
#include "sys.h"
#include "adc.h"
#include "eeprom.h"
#include "uart.h"
#include "battery.h"
#include "photo.h"


u16 battery_volt = 0;
u16 battery_test_count = 0;
u16 battery_limit =0;
u16 battery_fatal_limit =0;

#ifdef _CONFIGURE_LITHIUM
u8 Depass_Timer = DEPASSIVATION_INTERVAL;
u8 Depass_Pulse_Timer ;
#endif

/**************************************************************************************************************************
													Battery_Test_Current_On
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test_Current_On(void)
{
	_isgen = 1;
	_isgs1 = 1;
}

/**************************************************************************************************************************
													Battery_Test_Current_Off
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test_Current_Off(void)
{
	_isgen = 0;
	_isgs1 = 0;
}

/**************************************************************************************************************************
													Read_Battery_Volt
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
unsigned int Read_Battery_Volt(void)
{
	//u16 bat_volt_standby;
	u16 temp_bat_volt = 0;//,temp_bat_volt1 = 0;
	
	_pa4 = 1;
	_papu4 = 1;
	_pac4 = 1;
	_pawu4 = 0;
	_pas11 = 1;		/* Configure PA4 to  ad input */
	_pas10 = 0;
	
	_sadc0 = 0b00110111;
	_sadc1 = 0b00101100;
	_sadc1 = 0b00001011;
	//_sadc1 = 0b00000011;
	//_sadc0 = 0b00110010;
	_sadc0 = 0b00010000;
	_pa1 = 1;
	_adcen=1;
/*	_vbgren = 1;*/
	
	//30ms
	GCC_DELAY(30000);
	GCC_DELAY(30000);

	_start = 0;
	_start = 1;
	_start = 0;
	while(_adbz);
	adc_conv_data[1] = _sadoh;
	adc_conv_data[0] = _sadol;	
	_adcen=0;

	temp_bat_volt = (u16)adc_conv_data[1] << 8 | adc_conv_data[0];
	
	
	_pa1 = 0;		//BAT_TEST_OFF
	
	_pa4 = 1;
	_papu4 = 0;
	_pac4 = 0;
	_pawu4 = 0;
	_pa4 = 0;
	
	
//	Uart_Send_String("BAT 12BIT:");
//	_itoa((u16)temp_bat_volt,10);
//	Uart_Send_String("\r");
//	_itoa((u16)temp_bat_volt1,10);
//	Uart_Send_String("\r");


	return temp_bat_volt;
}

/**************************************************************************************************************************
													Battery_Pulse
*Description:
	Battery test, if battery voltage lower than 7.4~7.6VDC, enters low battery mode, this mode can be hush.
2 cycles for battery test, dwell time 20ms.
if battery voltage < 7.4V, it enters low battery fatal, this mode cannot be hushed.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Pulse(void)
{	

	u16 Battery_volt1,Battery_volt2;


	Battery_volt1 = Read_Battery_Volt();
	//Delay_Milliseconds(20);
	u16 i;
	for (i = 0; i < 20; i++)
	{
		GCC_DELAY(2000);	/*10 ms delay*/
	}
	Battery_volt2 = Read_Battery_Volt();
	
	//get max voltage
	if(Battery_volt2 >= Battery_volt1)
	{
		battery_volt = Battery_volt2;
	}	
	else
	{
		battery_volt = Battery_volt1;
	}
	
		
	//if((battery_volt > BATTERY_FATAL_LIMIT)&&(battery_volt <= BATTERY_WARNNING_LIMIT)) 
	if((battery_volt > battery_fatal_limit)&&(battery_volt <= battery_limit)) 
	{
		if((FLAG_BATTERY_LOW == FALSE) && (FLAG_BATTERY_HUSH_ACTIVE == FALSE))
		{
			FLAG_BATTERY_LOW = TRUE;
			FLAG_LB_HUSHABLE = TRUE;
			Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_LB_FAULT);			
		}
		Uart_Send_String("LB\r");
	}
	//else if(battery_volt <= BATTERY_FATAL_LIMIT){
	else if(battery_volt <= battery_fatal_limit)
	{
		FLAG_LB_HUSHABLE = FALSE;
		FLAG_BATTERY_LOW = TRUE;
		FLAG_BATTERY_HUSH_ACTIVE = FALSE;
		Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_LB_FAULT);
		Uart_Send_String("LB fatal\r");
	}else{
		FLAG_LB_HUSHABLE = FALSE;
		FLAG_BATTERY_LOW = FALSE;	
	}	

	//Uart_Send_Battery();
	Uart_Send_String(">B:\0");
	_itoa((u16) battery_volt, 10);
	Uart_Send_String(",L:\0");
	_itoa((u16) battery_limit, 10);
	Uart_Send_Byte('\r');	
}


/**************************************************************************************************************************
													Battery_Test
*Description:	When battery voltage going down to 7.4V~7.6V, enter low battery mode, in this mode the alarm will chirp every 60 seconds, 
				blink red LED every 30 seconds. The alarm can be hushed, hush will be timeout after 24 hours.When battery voltage < 7.4V, 
				this alarm cannot be hushed any more.
				In the 1st 12 minutes after power on , do the battery test every 1 minute, after 12 minutes, do battery test every 12 hours
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Battery_Test(void)
{
	/*static u16 battery_test_count = 0;*/
	static u8 _12_min_battery_test_ = 0;

	battery_test_count++;

	
	if(FLAG_POWER_ON) 
	{
		//if(battery_test_count >= _1_MINUTE_PERIOD)
		if(battery_test_count >= _BATTERY_PERIOD)
		{
			Battery_Pulse();
			battery_test_count = 0;
			if((_12_min_battery_test_ ++) >= _12_MIN_TIMEOUT)
			{
				FLAG_POWER_ON = FALSE;	
			}	
		}	
	}
	else
	{
		if( battery_test_count >= _12_HOURS)	// 12 hours check period.
		{
			battery_test_count = 0;	
			Battery_Pulse();
		}	
	}
}

/**************************************************************************************************************************
													vdd voltage read
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void VDD_Voltage_Read(void)
{
	u8 VDD_Voltage = 0;
//	u8 Compenstation_Smoke_Value = 0;
	
	_sadc0 = 0b00110111;
	_sadc1 = 0b00101100;
	_vbgren = 1;
	//_pa1 = 0;
	//30ms
	GCC_DELAY(30000);
	GCC_DELAY(30000);
	VDD_Voltage = Read_ADC_Channel(VBGREF,ADC_8BIT);
	//_pa1 = 1;
	_vbgren = 0;
	
//	Compenstation_Smoke_Value = (C_VDD_PARAMETER*raw_cav)/VDD_Voltage;
//	
//	Uart_Send_String("Compenstation_Smoke_Value:\0");
//	_itoa((u16) Compenstation_Smoke_Value, 10);
//	Uart_Send_Byte('\r');	
	
//	Uart_Send_String("VDD Voltage:\0");
//	_itoa((u16) VDD_Voltage, 10);
//	Uart_Send_Byte('\r');	
}




#ifdef _CONFIGURE_LITHIUM
	
#define NUM_DEPASS_PULSES	6
#define DEPASS_PULSE_TIME	5		// 500 ms in 100 ms tics

/**************************************************************************************************************************
													LFE_Check_Depassivation
*Description:
* This is called once a day
 * Every 30 days begin a Depassivation Cycle 
 * (set FLAG_DEPASSIVATION_ACTIVE, Init cycle
 * 
 * This triggers Active Mode Depass Timer (10 ms tics)
 *  and calls the LFE_Depassivation routine to execute
 *  six 500ms on / 500 ms off battery test pulses.
 * 
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void LFE_Check_Depassivation(void)
{   // Every 30 Days Execute depassivation cycle
    if(--Depass_Timer == 0)
    {
        FLAG_DEPASSIVATION_ACTIVE = 1;		// Also triggers active mode
        Depass_Timer = NUM_DEPASS_PULSES;  	// Use for Pulse Count
        Depass_Pulse_Timer = DEPASS_PULSE_TIME;

        BAT_TEST_ON     			// Start 1st Pulse to
        FLAG_DEPASSIVATION_ON = 1;		// Monitor Battery Test State
    }
	
}

/**************************************************************************************************************************
													LFE_Depassivation
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
// called every Active tic time
void LFE_Depassivation(void)
{
	static u8 lithium_counter = 0;
	static u8 wave_timer = 0;
			
	lithium_counter ++;
	if(lithium_counter >= 59)
	{
		lithium_counter = 0;
		if(FLAG_DEPASSIVATION_ON == 0)
		{
			FLAG_DEPASSIVATION_ON = 1;
           	BAT_TEST_ON	
		}
		else
		//if(FLAG_DEPASSIVATION_ON == 1)
		{
			FLAG_DEPASSIVATION_ON = 0;
           	BAT_TEST_OFF
           	Delay_Milliseconds(32);		//32MS
           	wave_timer ++;
            			
           	if(wave_timer >= 6)
			{
			   // Depassivation Completed!
			   FLAG_DEPASSIVATION_ACTIVE = 0;
			   FLAG_FAST_SAMPLING = FALSE;
			   // Re-Init 30 Day Interval
			   Depass_Timer = DEPASSIVATION_INTERVAL;
			}	
		}
	}
}

#endif