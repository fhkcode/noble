/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#ifndef	_BATTERY_H__
#define _BATTERY_H__

#define   	_CONFIGURE_LITHIUM			1
#define 	_VDS_COMPLIANT_

#ifdef _CONFIGURE_LITHIUM
#define DEPASSIVATION_INTERVAL  30	// in days

#define BAT_TEST_ON         _pa1 = 0;
#define BAT_TEST_OFF        _pa1 = 1;

#endif

/**************************************
Voltage compensation coefficient
Setting method: R measured according to normal working voltage_ VBG_ ADC value
For example, R at 3.0V_ VBG_ ADC is 102, and the coefficient is 102
For example, R at 3.6V_ VBG_ ADC is 85, and the coefficient is 85
**************************************/	
#define			C_VDD_PARAMETER		    102		//;	


// #define     _1_MIN_CNT				    60
#define	    _12_MIN_TIMEOUT			    	12
#define     _12_HOURS                   	(u16)43200

#define     BATTERY_WARNNING_LIMIT   		1000
#define     BATTERY_FATAL_LIMIT   			1000

#ifndef _VDS_COMPLIANT_
	#define	    LOW_BAT_HUSH_TIMEOUT			(u16)2800	// 24 hours timeout, 30 sec red led blink 24x60x2 = 2880
#else
	#define	    LOW_BAT_HUSH_TIMEOUT			(u16)1160	// 10 hours timeout, 30 sec red led blink 10x60x2 = 1200
	
#endif
//#define		VCC_WARNING_LIMIT			120	//120 2.55v
//#define		VCC_FATAL_LIMIT				123	//123 2.5v

#define BATTERY_FATAL_DELTA		50

extern u16 battery_test_count;

void Battery_Test_Current_On(void);
void Battery_Test_Current_Off(void);
unsigned int Read_Battery_Volt(void);
void Battery_Pulse(void);
void Battery_Test(void);
void VDD_Voltage_Read(void);
//void Uart_Send_Battery(void);

#ifdef _CONFIGURE_LITHIUM
void LFE_Check_Depassivation(void);
void LFE_Depassivation(void);

extern u16 battery_limit;
extern u16 battery_fatal_limit;
#endif

#endif