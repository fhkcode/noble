/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#include "board.h"
#include "sys.h"
#include "photo.h"
#include "horn.h"
#include "adc.h"
#include "eeprom.h"
#include "uart.h"
#include "battery.h"

#define 	SECTION_0		0
#define		SECTION_1		1
#define		SECTION_2		2
#define		SECTION_3		3

u8 prgm_data = 0;
u8 adc_conv_data[2];

u16 _life_cycles = 0;
u8 _life_seconds_counter = 0;
u16 _life_minutes_counter = 0;
u16 _life_speed_rate = 1;	//normal default set 1, life time speed rate is 1.

//byte_type	R_BOARD_FG = {.byte = 0};
	

/**************************************************************************************************************************
													Board_Init
*Description:	This function is to turn on the sounder with constone tone, for audibility testing.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Board_Init(void)
{

	battery_limit = Read_EE_Data(BAT_THRESD);
	battery_limit = (battery_limit<<8) | Read_EE_Data(BAT_THRESD + 1);
	
	battery_fatal_limit = battery_limit - BATTERY_FATAL_DELTA;
			
	photo_sampling_cnt = C_LONG_DELAY;
	Clear_Horn_Status();

	_life_cycles = (u16)(Read_EE_Data(LOC_LIFE_COUNT_HIGH) << 8) | (u16)Read_EE_Data(LOC_LIFE_COUNT_LOW);
}


/**************************************************************************************************************************
													Power_On_Beep
*Description:	This function turn on the sounder with a short beep sound, then blink the LED to notify the user that the 
				device runs Okay.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Power_On_Blink(void)
{
	Red_LED_Blink(30); /* LED blinks for 30 ms.*/
	Diagnostic_Event_Update(_life_cycles, LOC_MINIOR_RESET);
	if(FLAG_PHOTO_CAL == FALSE)
	{
		unsigned char i;
		
		for(i = 0;i < 30; i++)	//100ms * 30 = 3s, Red LED fast blink
		{
			GCC_CLRWDT();
			_RED_LED_ON();
			GCC_DELAY(100000);	//50ms
			_RED_LED_OFF();
			GCC_DELAY(100000);
		}
	}
}

/**************************************************************************************************************************
													S_RAM_Init
*Description:	This function is to clear the RAM data after power on, make sure all RAM are cleaned to "0".
				Holtek SOC has 2 RAM sectors, locates at address 0x80 ~ 0xFF. 
				Data can be addressed by MP1H and MP2L registers.
*Arguments: ram_sector	Sector number of RAM, should be "0" and "1"
*Return: 	None
***************************************************************************************************************************/
void S_RAM_Init(unsigned char ram_sector)
{
	u8 i = 0;
	for(i = 0; i < 8; i ++)
	{
		_mp1h = i;
		// _mp1l = C_RAM_ADDR_START;
		// _acc =  C_RAM_ADDR_END - C_RAM_ADDR_START + 1;
		_mp1l = 0x80;
		//_acc =  0x80;	/* 128 bytes for each sector, total 256 bytes. */
		_acc =  0xFF - 0x80 + 1;
		while( _acc != 0 )
		{
			_iar1 = 0;
			++_mp1l;
			_acc = _acc-1;	
		}	
	}

}

/**************************************************************************************************************************
													System_Init
*Description:	This function is to initialize the system requirements, like main clock, watchdog, IO port etc.
 				SCC[7:5]: system clock selection, 000 = fH, 0001 = fH/2....110 = fH/64
				[1]: FHIDEN: High Frequency oscillator control when CPU is switched off
				[0]: FSIDEN: Low Frequency oscillator control when CPU is switched off
				HIRCC[3:2]: Frequency selection: 00/11 = 2MHz, 01 = 4MHz, 10 = 8MHz	
					[1]: oscillator stable flag
					[0]: enable control
				prescaler register is for time base interrupts configuration.
					[1:0] prescaler clock source selection, 00 = fsys, 01 = fsys/4, 1x = fsub.
					sub clock = fLRC = 32KHz

				_pa, _pb		Port A/B input/output buffer
				_papu, _pbpu	Port A/B pull up register, 0: disable, 1: enable
				_pawu, _pbwu	Port A/B wake up register, 0: disable, 1: enable
				_pac, _pbc		Port A/B input output contorl register, 0: output, 1: input 

				For this 20SSOP SOC, PB0~PB3 are not pinout on the package.
				PA7	---	Test button, input
				PA6	---	Horn enable control, output, horn ENCLK physically connects to PA6, 0: enable, 1: disable, default 1
				PA5	---	Red LED, output
				PA4	---	BAT_VOLT, 
				PA3	---	Horn mode selection, output, horn mode physically connects to PA3, 0: 3 pin piezo selected, 1: 2 pin piezo selected, default 0.
				PA2	---	RX, ICPCK, input
				PA1	---	BAT_TEST,
				PA0	--- TX, ICPDA, output, default 0

				PB4	---	Horn control, output
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void System_Init(void)
{

	_scc	= 0;
	_hircc	= 0b00001001;	/*Set the main clock to 8MHz */
	while(!_hircf);			//wait until oscillator stable
	
	#if 1
//	_vlvd0 = 0;
//	_vlvd1 = 1;
//	_vlvd2 = 0;		//set lvr voltage is 2.4V
//	_vbgen = 1;		//enable bandgap 
//	_lvden = 1;		//enable lvr 
	_emi   = 1;		//Enable global interrupt
	_lvf   = 0;		//clear lvf interrupt
	_lve   = 1;	
/*	_lvden = 1;	*/
	
	_lvdc = 0b00011011;		// Bit 2~0  VLVD2~VLVD0:  001: 000: 2.0V?? 2.2V?? 010: 2.4V?? 011: 2.7V?? 100: 3.0V
	
	_wrf = 0;
	#endif
	_wdtc   = 0b01010101;	/*Watchdog verflow time to 2 sec */
	_hircen = 1;

/*
IO configuration:
* PxPUx - Pin pull-high function control, 0:Disable, 1:Enable
* PxWU - Wake-up function control, 0:Disable, 1:Enable
* PxCn -I/O Port x Pin type selection, 0: Output, 1: input
*/
	_pa    = 0b11110101;	/*IO Initialize  PA */
	_papu  = 0b10010100;	/*PA7, PA4 enable pull up resistor*/		
	_pac   = 0b10010100;	/*PA7(TEST BTN), PA4(BAT_VOLT) configured input, PA0,PA1(BAT_TEST),PA3,set output low, PA5(LED),PA6(PIEZO) set output high*/
	_pawu  = 0b10000000;	/*PA7 enable wake up function */
	
	
	_pb    = 0b00000010;	/*IO Initialize  PB */
	_pbpu  = 0b00000010; 	
	_pbc   = 0b00000010;	/*PB1(CONST TONE) PB3 configure as input*/
	_pbs0  = 0b00000000;

//	_pb    = 0b00000000;	/*IO Initialize  PB */
//	_pbpu  = 0b00000000; 	
//	_pbc   = 0b00000000;
//	_pbs0  = 0b00000000;

	S_RAM_Init(SECTION_0);	//2 RAM sectors, the data is to be cleared after power on. 


	//_sledc0 = 0b00000000;	//NOTE: [1:0] PA3~PA0 source current selection, 00 = Level 0 minimum.
	_sledc0 = 0;
			
//	_pas03 = 1;		/* Configure PA1 to Anolog input channel 2 -- PA1 = AN2. */
//	_pas02 = 0;
	_pas03 = 0;		/* Configure PA1 to Anolog input channel 2 -- PA1 = AN2. */
	_pas02 = 0;
	
	_pas07 = 0;
	_pas06 = 0;		/* Configure Pa3 to  input */
	_pbs03 = 0;		/* Configure Pb1 to  input */
	_pbs02 = 0;
	
	_pas11 = 1;		/* Configure PA4 to  ad input */
	_pas10 = 0;
	
	_pscr  =  0b00000011;

	_emi   = 1;		//Enable global interrupt
	
	_isgdata1 = 0;	// battery test current to 50mA
	
//	_pltsw = 0b00000000;	//disable plt
//	_pltdacc = 0b00000000;
	
/*	_pa1 = 1;		//BAT_TEST_OFF*/
	_pa1 = 0;		//BAT_TEST_OFF
	
	_pa4 = 1;
	_papu4 = 0;
	_pac4 = 0;
	_pawu4 = 0;
	_pa4 = 0;
	
}

/**************************************************************************************************************************
													Opamp_Init
*Description:	This function is to initialize the AFE circuit, the internal Opamp circuit, configure the bandwidth, gains 
				of amprefiers.
				1. Enable OPAMP0,bandwidth=2MH, enable OPAMP1,bandwidth=2MHz
				2. On:SDS5 SDS2 SDS1 SDS0
				3. Off: SDS6, SDS3
				3. R3(00=10K,01=20K,10=30K,11=40k),bit5~bit0  R2 ;6*100K, R1 ;15*100K
				When SDS6~SDS5 = 01, it's AC coupling mode.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Opamp_Init(void)
{
	_sda0c=	0b01000011;					
	_sda1c=	0b01000011;					
	
	if(_AC_COUPLING)
	{
		_sdsw=0b00100111;					
	}	
	else
	{
		_sdsw=0b01110111;
	}

	_sdpgac0 = eeprom._r1;		//0

	_sdpgac1 = eeprom._r2_r3;	//c6

	
}

/**************************************************************************************************************************
													Read_ADC
*Description:	This function is to start the 12-bit ADC unit, after conversion complete, read conversion data and update
				the global adc_conv_data, adc_conv_data is used on battery test, photo output reading.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Read_ADC(void)
{
	_start = 0;
	_start = 1;
	_start = 0;
	while(_adbz);
	adc_conv_data[1] = _sadoh;
	adc_conv_data[0] = _sadol;	
}

/**************************************************************************************************************************
													Opamp_0_Calibration
*Description:	Base on Holtek MCU data sheet, the internal Opamp have to be calibrated offset before using, just to make
				sure the offset will not be affected by the temperature, radiation or other causes.

	Note that if the SD Operational Amplifier inputs are pin-shared with I/O pins, they should be configured as the SD Operational 
	Amplifier input function before the Input Offset Calibration.
		Step 1. Set SDAnOFM=1 and SDAnRSP=1, the SD Operational Amplifier n is now under the input offset Calibration mode. To make sure the VAnOS as minimize as possible after calibration, the input reference voltage in calibration should be the same as input DC operating voltage in normal operation.
		Step 2. Set SDAnOF[5:0]=000000 and then read the SDAnO bit.
		Step 3. Increase the SDAnOF[5:0] value by 1 and then read the SDAnO bit.
		If the SDAnO bit state has not changed, then repeat Step 3 until the SDAnO bit state has changed.
		If the SDAnO bit state has changed, record the SDAnOF[5:0] value as VAnOS1 and then go to Step 4.
		Step 4. Set SDAnOF[5:0]=111111 and read the SDAnO bit.
		Step 5. Decrease the SDAnOF[5:0] value by 1 and then read the SDAnO bit.
		If the SDAnO bit state has not changed, then repeat Step 5 until the SDAnO bit state has changed.
		If the SDAnO bit state has changed, record the SDAnOF[5:0] value as VAnOS2 and then go to Step 6.
		Step 6. Restore the SD Operational Amplifier n input offset calibration value VAnOS into the SDAnOF[5:0] bit field. The offset Calibration procedure is now finished.
		VAnOS=(VAnOS1+VAnOS2)/2.
	If (VAnOS1+VAnOS2)/2 is not integral, discard the decimal.
	
	*Uncalibrated offset: ??15mV
	*Calibrated offset: ??2mV

*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Opamp_0_Calibration(void)
{
	unsigned char temp0 = 0,temp1 = 0;
	// temp0 = 0x00;
	// temp1 = 0x00;
	_sda0vos=0b11111111;
	_S_OPA_DELAY();
	while(_sda0o)
	{
		_sda0vos--;
		 _S_OPA_DELAY();	
		 	
	}	
	temp0=_sda0vos & 0b00111111;
	_sda0vos=0b11000000;
	_S_OPA_DELAY();
	while(!_sda0o)	 
	{
		_sda0vos++;
		_S_OPA_DELAY();
		
	}
	temp1=_sda0vos & 0b00111111;
	_sda0vos=(temp0 + temp1) / 2;

}

/**************************************************************************************************************************
													Opamp_1_Calibration
*Description:	Base on Holtek MCU data sheet, the internal Opamp have to be calibrated offset before using, just to make
				sure the offset will not be affected by the temperature, radiation or other causes.

	Note that if the SD Operational Amplifier inputs are pin-shared with I/O pins, they should be configured as the SD Operational 
	Amplifier input function before the Input Offset Calibration.
		Step 1. Set SDAnOFM=1 and SDAnRSP=1, the SD Operational Amplifier n is now under the input offset Calibration mode. To make sure the VAnOS as minimize as possible after calibration, the input reference voltage in calibration should be the same as input DC operating voltage in normal operation.
		Step 2. Set SDAnOF[5:0]=000000 and then read the SDAnO bit.
		Step 3. Increase the SDAnOF[5:0] value by 1 and then read the SDAnO bit.
		If the SDAnO bit state has not changed, then repeat Step 3 until the SDAnO bit state has changed.
		If the SDAnO bit state has changed, record the SDAnOF[5:0] value as VAnOS1 and then go to Step 4.
		Step 4. Set SDAnOF[5:0]=111111 and read the SDAnO bit.
		Step 5. Decrease the SDAnOF[5:0] value by 1 and then read the SDAnO bit.
		If the SDAnO bit state has not changed, then repeat Step 5 until the SDAnO bit state has changed.
		If the SDAnO bit state has changed, record the SDAnOF[5:0] value as VAnOS2 and then go to Step 6.
		Step 6. Restore the SD Operational Amplifier n input offset calibration value VAnOS into the SDAnOF[5:0] bit field. The offset Calibration procedure is now finished.
		VAnOS=(VAnOS1+VAnOS2)/2.
	If (VAnOS1+VAnOS2)/2 is not integral, discard the decimal.
	
	*Uncalibrated offset: ??15mV
	*Calibrated offset: ??2mV
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Opamp_1_Calibration(void)
{
	unsigned char temp0 = 0, temp1 = 0;
	// temp0 = 0x00;
	// temp1 = 0x00;
	_sda1vos = 0b11111111;
	_S_OPA_DELAY();
	while(_sda1o)
	{
		_sda1vos--;
		 _S_OPA_DELAY();	
	}	
	temp0=_sda1vos & 0b00111111;
	_sda1vos=0b11000000;
	_S_OPA_DELAY();
	while(!_sda1o)	 
	{
		_sda1vos++;
		_S_OPA_DELAY();
	}
	temp1=_sda1vos & 0b00111111;
	_sda1vos=(temp0 + temp1)/2;
}


/**************************************************************************************************************************
													Read_ROM_Data
*Description:	Read flash data to do the ROM data checksum calculation
				The Program Memory has a capacity of 4K??16 bits. The Program Memory is addressed by the Program Counter and also 
				contains data, table information and interrupt entries. Table data, which can be setup in any location within the 
				Program Memory, is addressed by a separate table pointer register.
*Arguments: addr	specific page
			Any location within the Program Memory can be defined as a look-up table where programmers can store fixed data. To
			 use the look-up table, the table pointer must first be setup by placing the address of the look up data to be retrieved 
			 in the table pointer registers, TBLP and TBHP. These registers define the total address of the look-up table.
			After setting up the table pointer, the table data can be retrieved from the Program Memory using the corresponding table 
			read instruction such as "TABRD [m]" or "TABRDL [m]" respectively when the memory [m] is located in sector 0.
			When the instruction is executed, the lower order table byte from the Program Memory will be transferred to the user defined 
			Data Memory register [m] as specified in the instruction. The higher order table data byte from the Program Memory will be 
			transferred to the TBLH special register.

*Return: 	None
***************************************************************************************************************************/
unsigned int Read_ROM_Data(unsigned int addr)
{
	u8 retval = 0;

	_tbhp = (addr>>8) & 0xFF;
	_tblp = addr & 0xFF;

	asm("TABRD _prgm_data");

	retval = _tblh;

	// retval = (retval << 8) | flash_data;

	return ((u16)retval + (u16)prgm_data);
}

/**************************************************************************************************************************
													ROM_Data_Validate
*Description:	To do ROM data validation, to make sure ROM data is complete and will not be corrupted.
*Arguments:	R_addrStart		Start address of ROM
			R_addrEnd		End address of ROM
*Return: 	None
***************************************************************************************************************************/
void ROM_Data_Validate(void)
{
	unsigned int addr;
	unsigned int ee_flash_sum = 0;
	u16 cal_sum = 0;
	// address = R_addrStart;
	
	for(addr = 0; addr< ROM_ADDR_RANGE + 1; addr++)
	{
		cal_sum += Read_ROM_Data(addr);
	}
	// Uart_Send_Ascii_Hex((cal_sum>>8));
	// Uart_Send_Ascii_Hex((cal_sum&0xff));
	ee_flash_sum = ((u16)Read_EE_Data(ROM_BKP_CHKSUM_ADDR_H)) << 8; 
	ee_flash_sum |= Read_EE_Data(ROM_BKP_CHKSUM_ADDR_L);
	
	
	if(ee_flash_sum == cal_sum)	//Calculate as the high 8 bits + low 8 bits of the data and save the result as 16 bits data
	{
		// FLAG_ROM_FAULT = FALSE;		
		// F_SYS_ROM_0_FAULT = FALSE;
		FLAG_ROM_FAULT = FALSE;
	}
	else
	{
		FLAG_ROM_FAULT = TRUE;
		Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_ROM_FAULT);
	}
}

/**************************************************************************************************************************
													Life_Update
*Description:	
*Arguments:		
*Return: 	None
***************************************************************************************************************************/
void Life_Update(void)
{
	u8 life_high = 0, life_low = 0;
	
	_life_seconds_counter++;
	
	if(_life_seconds_counter >= _1_MINUTE_PERIOD)
	{
		_life_seconds_counter = 0;
		
		#if 0
		Uart_Send_String("_life_cycles\r");
		_itoa((u16) _life_cycles, 10);
		#endif
		
		//if(FLAG_PHOTO_CAL)
		{
			if(_life_cycles >= LFE_EXPIRATION_DAY)	//3401
			{
				#if 0
				Uart_Send_String("FLAG_EOL\r");
				_itoa((u16) FLAG_EOL, 10);
				#endif
				if((FLAG_EOL == FALSE)  && (FLAG_EOL_HUSH == FALSE))
				{	
					FLAG_EOL = 1;
					Diagnostic_Event_Update(_life_cycles, LOG_MAJOR_EOL_FAULT);
					Uart_Send_String("\n");
					Uart_Send_String("EOL\r");
				}
			
				if(_life_cycles >= LFE_UL_EXTENSION)	//3408
				{
					FLAG_EOL = 0;
					FLAG_EOL_FATAL = 1;
				}
				else
				{
					FLAG_EOL_FATAL = 0;	
				}
			}
			else
			{
				FLAG_EOL = 0;	
				FLAG_EOL_FATAL = 0;	
			}
		}
		
		#if 0
		Uart_Send_String("_life_cycles\r");
		_itoa((u16) _life_cycles, 10);
		#endif
		
		_life_minutes_counter++;
		
		if(_life_minutes_counter >= (_1_DAY_MINUTES_/_life_speed_rate))
		{
			_life_minutes_counter = 0;
			_life_cycles ++;
			
			#ifdef _CONFIGURE_LITHIUM
            // Call Depassivation Check once a day
            LFE_Check_Depassivation();
        	#endif
			
			ROM_Data_Validate();		//7-20-2021  add  one day check rom
			EE_Data_Validate();
			

			life_high = (_life_cycles >> 8) & 0xFF;
			life_low =  _life_cycles & 0xFF;
	
			Write_EE_Data(LOC_LIFE_COUNT_HIGH, life_high);
			Write_EE_Data(LOC_LIFE_COUNT_LOW, life_low);

			Write_EE_Data(BACKUP_START + LOC_LIFE_COUNT_HIGH, life_high);
			Write_EE_Data(BACKUP_START + LOC_LIFE_COUNT_LOW, life_low);
			
			++eeprom._sum;
			Save_Checksum(eeprom._sum);
			// EE_Data_Validate(0);
		}
	}
}

/**************************************************************************************************************************
													EOL
*Description:	
*Arguments:	
			
*Return: 	None
***************************************************************************************************************************/
void EOL_Mode(void)
{
	if(_1_sec_counter >= _30_SECONDS_PERIOD)
	{
		_1_sec_counter = 0;	
		
		//if(FLAG_EOL_HUSH == FALSE)
		{
			unsigned char i;
			for(i=0;i<EOL_CODE;i++)
			{
				//Horn_Chirp_LED_Blink(LED_BLINK_WIDTH);
				Horn_Chirp(LED_BLINK_WIDTH);
				Red_LED_Blink(LED_BLINK_WIDTH);
				Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
			}
			Uart_Send_String("EOL\r");
		}
	}
}


/***************************************************************************************************************************
													EOL hush
*Description:	
*Arguments:	
			
*Return: 	None
***************************************************************************************************************************/
void EOL_HUSH_Mode(void)
{
	if(_1_sec_counter >= _30_SECONDS_PERIOD)
	{
		_1_sec_counter = 0;	
		
		_1_min_eol_hush_cnt ++;
		if(_1_min_eol_hush_cnt >= _1_Day_EOL_MINS_)
		{
			_1_min_eol_hush_cnt = 0;
			Uart_Send_String("EOL HUSH TIME\r");
			if(FLAG_EOL_HUSH == TRUE)
			{
				FLAG_EOL_HUSH = FALSE;
			}	
		}

		unsigned char i;
		for(i=0;i<EOL_CODE;i++)
		{
			Red_LED_Blink(LED_BLINK_WIDTH);
			Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
		}	
		Uart_Send_String("EOL HUSH\r");
	}
}

/**************************************************************************************************************************
													EOL_Fatal_Mode
*Description:	
*Arguments:	
			
*Return: 	None
***************************************************************************************************************************/
void EOL_Fatal_Mode(void)
{
	//_1_sec_counter = 0;
	
	
	if(_1_sec_counter >= _30_SECONDS_PERIOD)
	{
		_1_sec_counter = 0;	
		
		unsigned char i;
		for(i=0;i<EOL_CODE;i++)
		{
			//Horn_Chirp_LED_Blink(LED_BLINK_WIDTH);
			Horn_Chirp(LED_BLINK_WIDTH);
			Red_LED_Blink(LED_BLINK_WIDTH);
			Delay_Milliseconds(LED_BLINK_OFF_WIDTH);
		}
		
		Uart_Send_String("EOL Fatal\r");	
	}
	
}

