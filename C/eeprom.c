/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "sys.h"
#include "eeprom.h"
#include "BA45F5350.h"
#include "eeprom.h"
#include "board.h"
#include "uart.h"

struct Diag_Hist diagnostic_event;
_eeprom_struct eeprom= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// u8 ee_sum = 0;

//u8 _delta = 0;

void Load_Non_Volatile_data(void)
{
	u8 *pt = (u8*) &eeprom;
	u8 i;
	for(i=0; i<= LOC_CHECKSUM; i++)
	{
		*pt = Read_EE_Data(LOC_FACTORY_CAV + i);
		pt++;
	}
	// Uart_Send_String("validate ee data\r");
	EE_Data_Validate();
	

}

/**************************************************************************************************************************
													Read_EE_Data
*Description:	Read EEPROM data.
*Arguments:	
	addr		address of EEPROM to be read.
*Return: 	
	ee_data		data from specified address.
***************************************************************************************************************************/
u8 Read_EE_Data(u8 addr)
{
	//if((_lvdo == 0) && (_lvrf == 0))
	{
		u8  ee_data;
	
		_emi=0;
		_eea = addr;
		_mp1l = 0x40;
		_mp1h = 0x01;
		_iar1 =_iar1 | 0x03;
		while(_iar1 & 0x01);
		_iar1 = 0x00;
		_mp1h = 0x00;
		ee_data =_eed;
		_emi=1;
	
		return ee_data;	
	}	
}

/**************************************************************************************************************************
													Write_EE_Data
*Description:	Write data to specified address of EEPROM.
*Arguments:	
	addr		address of EEPROM
	_data		data to be written.
*Return: 	None
***************************************************************************************************************************/
void Write_EE_Data(u8 addr, u8 _data)
{
	//if((_lvdo == 0) && (_lvrf == 0))
	{
		_emi=0;
		_eea=addr;
		_eed=_data;
		_mp1l=0x40;
		_mp1h=0x01;
		_iar1=_iar1|0b00001000;
		_iar1=_iar1|0b00000100;
		while(_iar1&0b00000100) ;
		_iar1=0x00;
		_mp1h=0x00;
		_emi=1;
	}
}

u8 Calculate_Checksum(u8 start_addr)
{
	u8 val = 0;
	u8 sum = 0;
	
	u8 i = 0, zero_num = 0;

	u8 *pt = (u8*) &eeprom;

	//for(i=0; i< LOC_CHECKSUM; i++)
	for(i=0; i< PRIMARY_END; i++)
	{
		if(start_addr == BACKUP_START)
		{
			val = Read_EE_Data(start_addr + i);
		}
		else
		{
			// val = *pt;
			val = pt[i];
		}
		// val1 = *pt;
		if(val == 0x00)	/* database value validate, the data should not have 10 zeros in primay and backup database.*/
		{
			zero_num ++;
			if(zero_num > ZERO_NUMBER)
			{
				FLAG_MEMORY_FAULT = TRUE;
				return 0;
			}
		}
		else
		{
			sum += val;
		}

		// pt++;

	}
	return sum;
}

/**************************************************************************************************************************
													Copy_Database
*Description:	
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Copy_Database(u8 src, u8 dst)
{
	u8 i = 0;
	u8 val1 = 0;
	
	for(i=0; i<=LOC_CHECKSUM; i++)
	{
		val1 = Read_EE_Data(src + i);
		Write_EE_Data(dst + i, val1);
	}	
}

/**************************************************************************************************************************
													Save_Checksum
*Description:	
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Save_All_To_NV(void)
{
	u8 i=0;
	u8 *pt = (u8*) &eeprom;
	for(i=0; i<=PRIMARY_END; i++)
	{
		Write_EE_Data(PRIMARY_START + i, *pt);
		Write_EE_Data(BACKUP_START + i, *pt);
		pt++;
	}
}

/**************************************************************************************************************************
													Save_Checksum
*Description:	
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Save_Checksum(u8 sum)
{
	Write_EE_Data(LOC_CHECKSUM, sum);
	Write_EE_Data(BACKUP_END, sum);
}

/**************************************************************************************************************************
													EE_Data_Validate
*Description:	Validate EEPROM data
*Arguments:	update		0: update applied, update checksum, 1: no change
*Return: 	None
***************************************************************************************************************************/
void EE_Data_Validate()
{
	u8 sum = 0;
	
//	sum = Calculate_Checksum(LOC_FACTORY_CAV);
	sum = Calculate_Checksum(PRIMARY_START);
	
/*	Uart_Send_String("\ree data sum:\r.");*/
//	 _itoa(sum, 16);
/*	 _itoa((u16) sum, 16);*/
	 
	if( sum == eeprom._sum)
	{
		
		FLAG_EE_FAULT = FALSE;
	}
	else	/*if the primary calculated checksum does not equals to the checksum in database, then calculate the checkusm of backup database*/
	{			
		sum = Calculate_Checksum(BACKUP_START);	
		if(sum == Read_EE_Data(BACKUP_END))	/*if backup data is good, recover data from the backup database*/
		{

			Copy_Database(BACKUP_START, LOC_FACTORY_CAV);
			Diagnostic_Event_Update(_life_cycles, LOC_MINOR_EE_RECOVERY);
			FLAG_EE_FAULT = FALSE;
			Uart_Send_String("\rRecover EE data\r.");
		}
		else		/*the checksum of backup database is still wrong, then log a EE fault*/
		{
			// FLAG_EE_FAULT = TRUE;
			FLAG_EE_FAULT = TRUE;
			Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_EE_FAULT);
		}
	}
}

/**************************************************************************************************************************
													Diagnostic_Event_Init
*Description:	
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Diagnostic_Event_Init(void)
{
	u8 ptr = 0;

//	ptr = Read_EE_Data(0x2F);

	diagnostic_event._event_id = 0;
	
	/*major and minor event history offest check, in case the non-volatile memory data corrupted.*/
	ptr = Read_EE_Data(MAJOR_POINTER);
	if(ptr >= EVENT_NUMBERS)
	{
		diagnostic_event.major_ptr = 0;
	}
	else
	{
		//diagnostic_event.major_ptr = (ptr >> 4) & 0x0F;
		diagnostic_event.major_ptr = ptr;
	}
	
	ptr = Read_EE_Data(MINIOR_POINTER);
	if(ptr >= EVENT_NUMBERS)
	{
		diagnostic_event.minor_ptr = 0;
	}
	else
	{
		//diagnostic_event.minor_ptr = ptr & 0x0F;
		diagnostic_event.minor_ptr = ptr;
	}
	
}

/**************************************************************************************************************************
													Diagnostic_Event_Update
*Description:	Update dianostic events to the database, only 5 major and 5 minor events can be saved.
				Event history consists of 2 bytes day counter, 1 byte event type.
				event history format:
					xx 					xx 				xx
				low byte of day, high byte of day,	event type 

*Arguments:	day			life cycles.
			event_id	event type.

*Return: 	None
***************************************************************************************************************************/
void Diagnostic_Event_Update(u16 day, u8 event_id)
{

	diagnostic_event._event_id = event_id;
	
	u8 ptr_temp = 0;
	
	if(event_id >= MINIOR_EVENT_START)
	{
		
//		if((ptr_temp >= 0x30) && (ptr_temp <= 0x3E))
//		{
			Write_EE_Data(MINIOR_EVENT_START + diagnostic_event.minor_ptr++,  day & 0xFF);
			Write_EE_Data(MINIOR_EVENT_START + diagnostic_event.minor_ptr++,  (day >> 8) & 0xFF);
			Write_EE_Data(MINIOR_EVENT_START + diagnostic_event.minor_ptr++, diagnostic_event._event_id);	
		//}
	//	if(diagnostic_event.minor_ptr >= 0x0E)
		if(diagnostic_event.minor_ptr >= EVENT_NUMBERS)
		{
			diagnostic_event.minor_ptr = 0;
		}
		Write_EE_Data(MINIOR_POINTER, diagnostic_event.minor_ptr);
	}
	else
	{
		Write_EE_Data(MAJOR_EVENT_START + diagnostic_event.major_ptr++, day & 0xFF);
		Write_EE_Data(MAJOR_EVENT_START + diagnostic_event.major_ptr++,  (day >> 8) & 0xFF);
		Write_EE_Data(MAJOR_EVENT_START + diagnostic_event.major_ptr++, diagnostic_event._event_id);
		
	//	if(diagnostic_event.major_ptr >= 0x0E)
		if(diagnostic_event.major_ptr >= EVENT_NUMBERS)
		{
			diagnostic_event.major_ptr = 0;
		}
		Write_EE_Data(MAJOR_POINTER, diagnostic_event.major_ptr);
	}
	//Write_EE_Data(0x2F, (diagnostic_event.major_ptr<<4)|(diagnostic_event.minor_ptr));	/*Save the counter, in case recycle to update it.*/
}


/**************************************************************************************************************************
													Record Battery Voltages
*Description:	
* Record Battery Voltages
* Battery voltages are recorded at end of Day 1, Year 1 and
* at the end of each 24 hour period.
*
* Inputs - battime = (BAT_DAY_ONE, BAT_YEAR_ONE or BAT_CURRENT)
*          batvolt =  value to be recorded
*
* Outputs - none
*Arguments:	None
*Return: 	None

***************************************************************************************************************************/
//u8 Diag_Hist_Bat_Record(u8 battime, u16 batvolt)
//{
//    // Read Battery Record Row into buffer
//    Memory_Row_Read((u16)&DiagHist.Battery_Day_1);
//
//    switch(battime)
//    {
//
//        case BAT_DAY_ONE:
//            b1memorybuffer[ROW_OFFSET_BATTERY_DAY_1] = (u8)(batvolt & 0xff);
//            b1memorybuffer[ROW_OFFSET_BATTERY_DAY_1 + 1] = (u8)(batvolt >> 8);
//
//            // Always update the current battery Voltage
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT] = (u8)(batvolt & 0xff);
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT + 1] = (u8)(batvolt >> 8);
//            break;
//
//       case BAT_YEAR_ONE:
//            b1memorybuffer[ROW_OFFSET_BATTERY_YEAR_1] = (u8)(batvolt & 0xff);
//            b1memorybuffer[ROW_OFFSET_BATTERY_YEAR_1 + 1] = (u8)(batvolt >> 8);
//
//            // Always update the current battery Voltage
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT] = (u8)(batvolt & 0xff);
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT + 1] = (u8)(batvolt >> 8);
//            break;
//
//        case  BAT_CURRENT:
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT] = (u8)(batvolt & 0xff);
//            b1memorybuffer[ROW_OFFSET_BATTERY_CURRENT + 1] = (u8)(batvolt >> 8);
//            break;
//
//    }
//
//    //Row buffer has been updated, write back into flash memory
//    // All battery records are in same row as Battery_Day_1 record
//    Memory_Row_Erase((u16)&DiagHist.Battery_Day_1);
//
//    // Does not always work OK with call to write verify row
//    // depending upon location in memory (if i add debug instructions
//    // it works OK. acts like compiler may be causing stack overflows
//    u8 result = Memory_Write_Verify((u16)&DiagHist.Battery_Day_1);
//
//    // Works OK with the direct call to write row
//    //UCHAR result = Memory_Row_Write((UINT)&DiagHist.Battery_Day_1);
//
//    if(result == FLASH_FAIL)
//    {
//        //SER_Send_String("Fail");
//        //Flash Write Failure - Abort write to flash
//        return FLASH_FAIL;
//    }
//    else
//    {
//        //SER_Send_String("Pass ");
//        return FLASH_SUCCESS;
//    }
//}