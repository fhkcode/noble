/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/


#ifndef	_A_EEPROM_LIST_H__
#define _A_EEPROM_LIST_H__

struct Diag_Hist
{
    /* data */
   // u16 _day;
    u8 _event_id;
    u8 major_ptr;
    u8 minor_ptr;
    // void (* update)(u16, u8);
};

struct _eeprom
{
    /* data */
    u8 _factory_cav;
    u8 _cal_complete;
    u8 _ptt_current;
    u8 _alarm_thd;
    u8 _corrected_alarm_thd;
    u8 _ptt_error;
    u8 _photo_supv;
    u8 _r1;
    u8 _r2_r3;
    u8 _ired_current;
    u8 _battery;
    u8 _life_low;
    u8 _life_high;
    u8 _delta;
    // u8 _cal_complete;
    u8 _sum;

};



typedef struct _eeprom _eeprom_struct;

#define		LOC_FACTORY_CAV					0x00	//CAC At Calibration
#define		LOC_SMOKE_CAL_COMPLETE			0x01	
#define		LOC_PTT_DRIVE_CURRENT			0x02	//IRED LED PTT Drive Current
#define		LOC_ALARM_THRESHOLD				0x03	//Alarm Threshold Factory Value
#define		LOC_CORRECTED_ALM_THRESHOLD		0x04	//Corrected Alarm Threshold
#define		LOC_PTT_ERROR					0x05	//PTT Threshold
#define		LOC_PHOTO_SUPV_LIMIT			0x06	//Photo supervision value	
#define		LOC_SDPGAC0			            0x07	//SDPGAC0 (R1)
#define		LOC_SDPGAC1			            0x08	//SDPGAC1 (R2, R3)
#define		LOC_ISGDATA			            0x09	//IRED LED Drive Current
#define		LOC_UNUSED		            	0x0A	
#define     LOC_LIFE_COUNT_LOW              0x0B
#define     LOC_LIFE_COUNT_HIGH             0x0C
#define		LOG_DELTA						0x0D
#define		LOC_CHECKSUM				    0x0E	//Checksum

#define		PRIMARY_START					0x00
#define		PRIMARY_END						0x0E
//#define		BACKUP_START				    0x10 + 0x00
//#define		BACKUP_END					    0x10 + LOC_CHECKSUM	// 0x1E 0x1D
#define		BACKUP_START				    0x10 + PRIMARY_START
#define		BACKUP_END					    0x10 + PRIMARY_END	// 0x1E

#define     ZERO_NUMBER                     10

//Backup location for EEPROM and ROM checksum value
#define		ROM_BKP_CHKSUM_ADDR_H           0x0F
#define		ROM_BKP_CHKSUM_ADDR_L           0x1F

#define		MAJOR_EVENT_START				0x20
#define		MINIOR_EVENT_START				0x40

#define     LOC_MAJOR_PHOTO_FAULT           (MAJOR_EVENT_START + 0x00)
#define     LOC_MAJOR_LB_FAULT              (MAJOR_EVENT_START + 0x01)
#define     LOC_MAJOR_EE_FAULT              (MAJOR_EVENT_START + 0x02)
#define     LOC_MAJOR_ROM_FAULT             (MAJOR_EVENT_START + 0x03)
#define     LOC_MAJOR_DFRIT_COMP_FAULT      (MAJOR_EVENT_START + 0x04)
#define     LOC_MAJOR_SMOKE_ALARM           (MAJOR_EVENT_START + 0x05)
#define     LOC_MAJOR_PTT_FAULT             (MAJOR_EVENT_START + 0x06)
#define		LOG_MAJOR_EOL_FAULT				(MAJOR_EVENT_START + 0x07)

#define     LOC_MINIOR_RESET                (MINIOR_EVENT_START + 0x00)
#define		LOC_MINIOR_SMOKE_HUSH			(MINIOR_EVENT_START + 0x02)
#define     LOC_MINOR_EE_RECOVERY           (MINIOR_EVENT_START + 0x03)

//#define     LOC_MAJOR_PHOTO_FAULT           0x20
//#define     LOC_MAJOR_LB_FAULT              0x21
//#define     LOC_MAJOR_EE_FAULT              0x22
//#define     LOC_MAJOR_ROM_FAULT             0x23
//#define     LOC_MAJOR_DFRIT_COMP_FAULT      0x24
//#define     LOC_MAJOR_SMOKE_ALARM           0x25
//#define     LOC_MAJOR_PTT_FAULT             0x26
//#define		LOG_MAJOR_EOL_FAULT				0x27
//
//#define     LOC_MINIOR_RESET                0x30
//#define     LOC_MINIOR_PTT_NUMBERS          0x3F
//#define		LOC_MINIOR_SMOKE_HUSH			0x32
//#define     LOC_MINOR_EE_RECOVERY           0x33


#define		EVENT_NUMBERS					0x1E
#define		MINIOR_POINTER					0x70
#define		MAJOR_POINTER					0x71
#define		BAT_THRESD						0x72
#define		LOG_BATTERY_DAY					0x74
#define		LOG_BATTERY_YEAR				0x76
#define     LOC_MINIOR_PTT_NUMBERS          0x78	

extern struct Diag_Hist diagnostic_event;
extern u8 ee_sum;
extern _eeprom_struct eeprom;

void Load_Non_Volatile_data(void);
u8 Read_EE_Data(u8 addr);
void Write_EE_Data(u8 addr,u8 _data);
u8 Calculate_Checksum(u8 start_addr);
void Copy_Database(u8 src, u8 dst);
void Save_All_To_NV(void);
void Save_Checksum(u8 sum);
void EE_Data_Validate();
void Diagnostic_Event_Init(void);
void Diagnostic_Event_Update(u16 day, u8 event_id);

#endif


