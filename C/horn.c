/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "horn.h"
//#include "A_BA45F5350.h"
#include "board.h"
#include "sys.h"
#include "button.h"
// #include "led.h"

byte_type	flag_horn_alarm_bits = {.byte = 0};

//REVIEW: define T3 pattern cycles, after 2 cycles of T3 pattern, force the MCU to reset.
#define		PTT_T3_PATTERN_CYCLES		2

/**************************************************************************************************************************
													Horn_Init
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Horn_Init(void)
{

#if _HORN_DRIVE_MODE_
	_pas15 = 0;		/* _pas15, _pas14 configur PA6 as general IO*/
	_pas14 = 0;		
	_pa6 = 1;	/* Output 1 to disable horn */
	_pac6 = 0;	/* configure PA6 as output */
//	_pa3 = 0;	/* PA3 physically conects to horn mode selection, 0: 3 pin piezo, 1: 2 pin piezo */
#else
	_pas15 = 0;
	_pas14 = 1;	
	_pa3 = 1;
#endif		
	_pton  = 0;
	
}

/**************************************************************************************************************************
													Horn_T3_Pattern
*Description:	This function is to provide T3 patterns based on smoke alrm signal or push-to-test signal.
				After push-to-test complete, firmware will be force to reset to clear all flags.
				
				A smoke alarm that produces an audible signal which is intended to initiate immediate evacuation
				from the protected area shall produce the signal in the form of the �three pulse?temporal pattern shown
				in Figure 38.1. Each ON phase shall last 0.5 second ?0 percent followed by an OFF phase of 0.5 second
				?0 percent. After the third of these ON phases, there shall be an OFF phase that lasts 1.5 seconds ?0
				percent. Where the intended action is not immediate evacuation, the audible signal shall produce an alert
				signal distinctive from the �three pulse?temporal system.

*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Horn_T3_Pattern(void)
{
	static u8 horn_on_off_delay = 0;
	static u8 horn_pulse_cnt = 0;
	static u8 ptt_alarm_reset_cnt = 0;
	
	if(FLAG_HORN_ALARM_ENABLE)
	{
		if(FLAG_T3_ALARM_BEGIN == FALSE)
		{
			horn_pulse_cnt = SMOKE_ALARM_PATTERN;
			photo_sampling_cnt = C_LONG_SP_DELAY; // 90 * 8.192ms = 737ms
			FLAG_T3_ALARM_BEGIN = TRUE;
			FLAG_T3_ALARM_ON = TRUE;
			horn_on_off_delay = HORN_ALARM_ON_DURATION;
			if(FLAG_PTT_ACTIVE || FLAG_HUSH_ACTIVE == FALSE)
			{
				_RED_LED_ON();
				_OPEN_BUZZ();
				_pa6 = 0;
				GCC_DELAY(5*2000);
				_pa6 = 1;
				GCC_DELAY(5*2000);
				_pa6 = 0;
			//	ptt_alarm_reset_cnt = 0;
			}
		}
		else
		{
			--horn_on_off_delay;
			if(horn_on_off_delay == 0)
			{
				if(FLAG_T3_ALARM_ON == FALSE)
				{
					FLAG_T3_ALARM_ON = TRUE;
					horn_on_off_delay = HORN_ALARM_ON_DURATION;
					if(FLAG_PTT_ACTIVE || FLAG_HUSH_ACTIVE == FALSE)
					{
						_RED_LED_ON();
						_OPEN_BUZZ();
						_pa6 = 0;
						GCC_DELAY(5*2000);
						_pa6 = 1;
						GCC_DELAY(5*2000);
						_pa6 = 0;
					}
				}
				else
				{
					FLAG_T3_ALARM_ON = FALSE;
					horn_on_off_delay = HORN_ALARM_OFF_DURATION;
					_RED_LED_OFF();	
					_OFF_BUZZ();
					--horn_pulse_cnt;
					if(horn_pulse_cnt == 0)		/*T3 cyceles check.*/
					{
						horn_pulse_cnt = SMOKE_ALARM_PATTERN;
						horn_on_off_delay = HORN_ALARM_DWELL_DURATION;
						
						if(FLAG_T3_ALARM_STOP)
						{
							flag_horn_alarm_bits.byte = 0;
						}

						//NOTE After 2 T3 patterns, Use watchdog to force firmware reset, this reset is a hardware reset.
						if(FLAG_PTT_ACTIVE)	//NOTE: this is only for PTT test.	
						{	
							ptt_alarm_reset_cnt++;
							if(ptt_alarm_reset_cnt >= PTT_T3_PATTERN_CYCLES)
							{
								FLAG_PTT_ACTIVE = FALSE;
								_pa6 = 1;
								Delay_Milliseconds(800);
								_wdtc = 0;	// Watchdog reset.
								while(TRUE);
							}
						}
				/* Push-to-Test Check*/
					}
				}
			}
		}
	}
}
