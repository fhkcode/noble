/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/


#ifndef _HORN_H__
#define _HORN_H__

#include "BA45F5350.h"
#include "sys.h"	

#define	    _HORN_DRIVE_MODE_		        1		//1 = 3 pin piezo, 0 = 2 pin piezo
#define     HORN_ALARM_ON_DURATION   	    61		// Horn T3 On duration	N * 8.192 ms
#define     HORN_ALARM_OFF_DURATION   	    61		// Horn T3 Off duration	N * 8.192 ms
#define     HORN_ALARM_DWELL_DURATION       183		// DwElling time N * 8.192 ms
#define     SMOKE_ALARM_PATTERN		        3		// Horn repeat times, consists of 2 T3 cycles.
// #define	    HORN_FREQUENCY	                (u16)625		//freuency set to 3.2k
// #define     HORN_PULSE_DUTY 	            (u16)312			//Duty 50%
#define		PTT_T3_PATTERN_CYCLES		    2

extern byte_type flag_horn_alarm_bits;
#define		FLAG_HORN_ALARM_ENABLE		flag_horn_alarm_bits.bits.bit0
#define		FLAG_T3_ALARM_BEGIN			flag_horn_alarm_bits.bits.bit1	
#define		FLAG_T3_ALARM_ON			flag_horn_alarm_bits.bits.bit2
#define		FLAG_T3_ALARM_STOP			flag_horn_alarm_bits.bits.bit3

#define	    _OPEN_BUZZ()	{if (_HORN_DRIVE_MODE_) { _pa6 = 0;} else {_pton = 1;}}	//_pton = 1
#define	    _OFF_BUZZ()		{if (_HORN_DRIVE_MODE_) { _pa6 = 1;} else {_pton = 0;}}	//_pton = 0

void Horn_Init(void);
void Horn_T3_Pattern(void);

#endif