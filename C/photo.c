/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "photo.h"
#include "board.h"
#include "eeprom.h"
#include "uart.h"
#include "sys.h"
#include "adc.h"
#include "horn.h"

extern unsigned int _1_min_alarm_memory_cnt;
u8 _2hrs_avg_cav = 0;
u8 pre_alarm_condition = 0;
u8 exit_alarm_condition = 0;
u8 ptt_raw_cav = 0;
u8 raw_cav = 0;
//u8 init_cav = 0;
//u8 moving_avg[4];
//u8 photo_cal_sampling_cnt = 0;
u8 photo_cal_upper_lvl = 0;
u8 photo_cal_lower_lvl = 0;
u16 drift_sec_counter = 0;
u8	calibration_state = 0;
u8 	slope = 0;
u8  calibration_300mA_cav = 0;
u8 max_drift_compst = 0;


/**************************************************************************************************************************
													Photo_Params_Init
*Description:	Check the LOC_FACTORY_CAV if it's empty (0x00) or not, to validate alarm device if calibrated or not. Then if 
				calibration is done, initialize the alarm parameters, like alarm thresold, supervision limits etc.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Photo_Params_Init(void)
{
	
	photo_cal_lower_lvl = PHOTO_CAL_LOWER_LEVEL;	
	photo_cal_upper_lvl = PHOTO_CAL_UPPER_LEVEL;

	AFE_Init();

//	if(eeprom._cal_complete == 0x01)	/*Calibration done*/
	if(eeprom._cal_complete == 0x03)
	{
		FLAG_PHOTO_CAL = TRUE;

		_2hrs_avg_cav = eeprom._factory_cav;
		pre_alarm_condition = (u8)((float)eeprom._corrected_alarm_thd * LEVEL_PRE_ALARM);
		exit_alarm_condition =  (u8)((float)eeprom._corrected_alarm_thd * LEVEL_EXIT_ALARM);
		max_drift_compst = (u8)((float)eeprom._corrected_alarm_thd * LEVEL_DRIFT);
	}
//	else if(eeprom._cal_complete == 0x00)	/*Calibration is not done*/
	else if(eeprom._cal_complete < 0x02)
	{
		FLAG_PHOTO_CAL = FALSE;
		FLAG_IN_CAL_STATUS = TRUE;
//		if( eeprom._cal_complete == 0x00)
//		{
//			Uart_Send_String("\rlow battery cal\r.");
//		}
//		if( eeprom._cal_complete == 0x01)
//		{
//			Uart_Send_String("\rsmoke cal\r.");
//		}		
	}
	else	/*Other value could be considered as data corrupted, go to non volatile data fault.*/
	{
		FLAG_EE_FAULT = TRUE;	/*FW will go to memory fault in the Power on output function...*/ 
	}
}

/**************************************************************************************************************************
													All_OPamps_Enabled
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void All_OPamps_Enabled(void)
{
	_sda0en	= 1;
	_sda1en = 1;
	_sds0   = 1;
}

/**************************************************************************************************************************
													All_OPamps_Disabled
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void All_OPamps_Disabled(void)
{
	_sda0en = 0;
	_sda1en = 0;
	_sds0   = 0;
}

/**************************************************************************************************************************
													IRED_LED_On
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void IRED_LED_On(void)
{
	_isgen = 1;
	_isgs0 = 1;
}

/**************************************************************************************************************************
													IRED_LED_Off
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void IRED_LED_Off(void)
{
	_isgen = 0;
	_isgs0 = 0;
}


/**************************************************************************************************************************
													IRED_Standby_Set
*Description:	Under normal mode, the IRED LED drive current will be set to 200mA.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
//void IRED_Standby_Set(void)
void IRED_Standby_Set(u8 Current)
{
//	_sdpgac0 = Read_EE_Data(LOC_SDPGAC0);
//	_sdpgac1 = Read_EE_Data(LOC_SDPGAC1);
	if(Current == 0)
	{
		_isgdata0 = Read_EE_Data(LOC_ISGDATA);
	}
	else
	{
		_isgdata0 = Read_EE_Data(LOC_ISGDATA) + 10;//15  350mA	;   25 300mA
	}
}

/**************************************************************************************************************************
													IRED_Ptt_Set
*Description:	Set IRED drive current to 300mA, if the S count change > 10, then the Push-to-Test is valid, otherwise, fails.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void IRED_Ptt_Set(void)
{
	// _isgdata0 = Read_EE_Data(LOC_PTT_DRIVE_CURRENT);
	_isgdata0 = eeprom._ptt_current;
}


/**************************************************************************************************************************
													AFE_Init
*Description:
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void AFE_Init(void)
{

	// All_OPamps_Enabled();
	IRED_LED_Off();
	All_OPamps_Disabled();
	GCC_DELAY(400);	 //  delay 200us
	Opamp_Init();
	//GCC_DELAY(400); //  delay 200us

}

/**************************************************************************************************************************
													Read_Photo_Output
*Description:	Turn on the IRED LED 130us, in the meanwhile, read the photo output and convert the anolog signal to digital
				S value.
				Period of sampling is 10 seconds under standby.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Read_Photo_Output(void)
{
//	static u8 high_temp_alarm_flag = 1;
	
	if(FLAG_PHOTO_CAL && FLAG_PTT_ACTIVE)
	{
		IRED_Ptt_Set();
	}
	else
	{
		if(calibration_state == 1)
		{
			IRED_Standby_Set(Current_300mA);
		}
		else
		{
			IRED_Standby_Set(Current_200mA);	
		}
		
	}
		
	_emi = 0;	/*disable global interrupt to protect the voltage from affecting by serial communication*/

	All_OPamps_Enabled();
	GCC_DELAY(1000);

	IRED_LED_On();
	GCC_DELAY(132);
	
	if(FLAG_PHOTO_CAL && FLAG_PTT_ACTIVE)
	{
		ptt_raw_cav = Read_ADC_Channel(OPA1O, ADC_8BIT);
	}
	else
	{
		raw_cav = Read_ADC_Channel(OPA1O, ADC_8BIT);
	}
	IRED_LED_Off();
	

	All_OPamps_Disabled();

	_emi = 1;	/*enble global interrupt*/

	if(FLAG_PHOTO_CAL)
	{
		if((raw_cav < eeprom._photo_supv))		//photo fault
		{
			if((FLAG_PHOTO_FAULT == FALSE)&&(FLAG_PTT_ACTIVE == 0 ) )//&& (FLAG_PTT_ACTIVE == 0 )
			{
				Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_PHOTO_FAULT);
				FLAG_PHOTO_FAULT = TRUE;
			}
		}
		else
		{
			FLAG_PHOTO_FAULT = FALSE;
		}
	}
	Uart_Send_String(">S:\0");
	_itoa((u16) raw_cav, 10);
	if(FLAG_SMOKE_ALARM)
	{
		Uart_Send_Byte('!');
	}
	else if(FLAG_HUSH_ACTIVE)
	{
		Uart_Send_Byte('"');
	}
	Uart_Send_Byte('\r');
}

/**************************************************************************************************************************
													Smoke_Calibration
*Description:	The smoke calibration process is to sample 30 photo outputs in 30 seconds, with a sample rate of 1 count per
				second. And then take an average of these 30 samples, the alarm threshold then is calculted by averaged CAV + 60.
	Algorithm:
				Alarm threshold = Average CAV + 60.
	the Average CAV will be saved to the EEPROM as Factory CAV. 
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Smoke_Calibration(void)
{
	u16 slope10_6 = 0;
	u16 Valarm = 0;
	
	static u8 sample_cnt =0;
	static u16 cav_sum = 0;
	
	switch(calibration_state)
	{		
		case 0:
				cav_sum += raw_cav;
				sample_cnt ++;
				if(sample_cnt >= CAL_15_AVERAGE_CAV)
				{
					sample_cnt = 0;
					
					eeprom._factory_cav = cav_sum / CAL_15_AVERAGE_CAV;
					
//					Uart_Send_String("200mA_cav, "); 
//					_itoa((u16)eeprom._factory_cav, 10);
//					Uart_Send_String("\r"); 
					
					calibration_state ++;
					cav_sum = 0;
				}
		
		break;
		case 1:	
				cav_sum += raw_cav;
				sample_cnt ++;
				if(sample_cnt >= CAL_15_AVERAGE_CAV)
				{
					sample_cnt = 0;
					
					calibration_300mA_cav = cav_sum / CAL_15_AVERAGE_CAV;
					
					slope10_6 = (u16)(((calibration_300mA_cav - eeprom._factory_cav) << 6)/100);
					Valarm = (SMK_ALARM_OFFSET_SCALE_FHK*eeprom._factory_cav)/100 - ((SMK_ALARM_SENS_SCALE_FHK*slope10_6) >> 6);
					Valarm = (Valarm*10 -SMK_ALARM_OFFSET_FHK)/10;	
					eeprom._alarm_thd = Valarm;

					
					calibration_state ++;
					cav_sum = 0;
				}
		break;
		
		case 2:
				if(((eeprom._factory_cav < photo_cal_upper_lvl) && (eeprom._factory_cav > photo_cal_lower_lvl)) && 
				((calibration_300mA_cav< (photo_cal_upper_lvl+50)) && (calibration_300mA_cav > photo_cal_lower_lvl)))  // check if inital cav is within limits
				{
					FLAG_PHOTO_CAL = TRUE;			//Calibraiton done
					FLAG_PHOTO_CAL_FAILURE = FALSE;
					FLAG_STANDBY = TRUE;				//Standby mode
					FLAG_IN_CAL_STATUS = FALSE;
		
					//eeprom._alarm_thd = eeprom._factory_cav + DELTA_THRESHOLD;
					//eeprom._alarm_thd = Valarm;
					eeprom._corrected_alarm_thd = eeprom._alarm_thd;
					eeprom._delta = eeprom._corrected_alarm_thd - eeprom._factory_cav;
					//_delta = eeprom._corrected_alarm_thd - eeprom._factory_cav;	
					eeprom._photo_supv = (eeprom._factory_cav * 64) / 100;
			
				//	eeprom._cal_complete = 0x01;
					eeprom._cal_complete = 0x03;
		
					eeprom._sum = Calculate_Checksum(LOC_FACTORY_CAV);
		
					Save_All_To_NV();
					
					Uart_Send_String("Cal done, "); 
					_itoa((u16)eeprom._factory_cav, 10);
				//	Uart_Send_String("\r"); 
					Uart_Send_String(", "); 
					_itoa((u16)calibration_300mA_cav, 10);
				//	Uart_Send_String("\r");
					Uart_Send_String(", ");  
					_itoa((u16)eeprom._alarm_thd, 10);
					Uart_Send_String("\r"); 
//					Uart_Send_String("Valarm:"); 
//					_itoa(Valarm, 10);
//					Uart_Send_String("\r"); 

		
					Delay_Milliseconds(800);
					_wdtc = 0;	// force to reset.
					while(TRUE);
				}
				else
				{
					FLAG_PHOTO_CAL = FALSE;
					FLAG_PHOTO_CAL_FAILURE = TRUE;
					FLAG_IN_CAL_STATUS = FALSE;
					Uart_Send_String("Cal failed, "); 
//					_itoa((u16)eeprom._factory_cav, 10);
//					Uart_Send_String("\r"); 
				}
//				_itoa((u16)eeprom._factory_cav, 10);
//				Uart_Send_String("\r"); 
		break;
		
		default:
		break;
			
	}
}

/**************************************************************************************************************************
													Smoke_Alarm_Check
*Description: This function is to check the if the photo output is over the alarm threshold, and turn on the horn to give
			T3 pattern.
			if 3 successive photo output are over the alarm threshold, then a alarm event will be valid, otherwise it will
			be considered as a noise.
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Smoke_Alarm_Check(void)
{
	static u8 Alarm_Sample_Cnt = 0;

	if((raw_cav >= eeprom._corrected_alarm_thd) && (FLAG_SMOKE_ALARM == FALSE) && (FLAG_HUSH_ACTIVE == FALSE))
	{ 
	//	FLAG_FAST_SAMPLING = TRUE;
		FLAG_PRE_ALARM = TRUE;
		Alarm_Sample_Cnt ++;
		if(Alarm_Sample_Cnt >= ALARM_CONFIRM_CNT) //alarm activate
		{
			Alarm_Sample_Cnt = 0;

			FLAG_SMOKE_ALARM = TRUE;	
			FLAG_HORN_ALARM_ENABLE = TRUE;
			FLAG_T3_ALARM_STOP = FALSE;
			FLAG_PRE_ALARM = FALSE;
			FLAG_STANDBY = FALSE;
			FLAG_ALARM_MEMORY = FALSE;
			FLAG_PTT_ACTIVE = FALSE;
			Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_SMOKE_ALARM);
			Uart_Send_String("Smoke alarm!\r");
		}		
	}
	else if((raw_cav < exit_alarm_condition) && (FLAG_SMOKE_ALARM)) // alarm cancle.
	{
		FLAG_T3_ALARM_STOP = TRUE;
		FLAG_SMOKE_ALARM = FALSE;	
		FLAG_ALARM_MEMORY = TRUE;	// exit alarm mode, then enter alarm memory.
		_1_sec_alarm_memory_cnt = 0;
		// _1_min_alarm_memory_cnt = 0;
		Alarm_Sample_Cnt = 0;
		Uart_Send_String("Alarm memory\r");
	}
	else if((raw_cav >= pre_alarm_condition) && (raw_cav < eeprom._corrected_alarm_thd) && (FLAG_SMOKE_ALARM == FALSE) && (FLAG_PRE_ALARM == FALSE)) //pre-alarm activate
	{				
		FLAG_PRE_ALARM = TRUE;
		FLAG_STANDBY = FALSE;
		
		Alarm_Sample_Cnt = 0;
		// Uart_Send_String("Pre-alarm\r");
	}
	else if(raw_cav < pre_alarm_condition){
		FLAG_PRE_ALARM = FALSE;
		Alarm_Sample_Cnt = 0;	
	}
}

/**************************************************************************************************************************
													Push_To_Test_Check
*Description: 	If the S value under 300mA is greater than 15 or more than the standby S value (200mA drive), 
				the PTT is considered valid, otherwise the PTT error mode is entered.

				The Push-to-Test will take a reference from the _2hrs_avg_cav variant, if the 300mA make an increment above 10
				counts, then consider the PTT test is valid, otherwise it fails.
				
				As the photo output will be decayed in the high temperature, normall have 20 counts dropped, so the Push-to-Test
				will fail in some worst case.
				
*Arguments: None
*Return: 	None
***************************************************************************************************************************/
void Push_To_Test_Check(void){

	if((ptt_raw_cav > raw_cav) && (((int)(ptt_raw_cav - raw_cav)) > eeprom._ptt_error))	
	{
		if(F_ALARM_MODE == FALSE)
		{
			Clear_Horn_Status();	
			F_ALARM_MODE = TRUE;
			FLAG_T3_ALARM_STOP = FALSE;
			FLAG_HORN_ALARM_ENABLE = TRUE;
			// Uart_Send_String("Delta S:");
		//#if _DEBUG_
		#if 0
			_itoa((u16)(raw_cav), 10);
			Uart_Send_String(", ");
			_itoa((u16)(eeprom._factory_cav), 10);
			Uart_Send_String(", ");
			_itoa((u16)(raw_cav - eeprom._factory_cav), 10);
			Uart_Send_Byte('\r');
		#endif
		}
	}
	else
	{
		// FLAG_PHOTO_FAULT = TRUE;
		FLAG_PTT_ACTIVE = FALSE;
		FLAG_T3_ALARM_STOP = TRUE;
		//FLAG_HORN_ALARM_ENABLE = FALSE;
		FLAG_PTT_FAULT = TRUE;
		Clear_Horn_Status();
		Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_PTT_FAULT);

		_itoa((u16)(raw_cav), 10);
		Uart_Send_String(", ");
		_itoa((u16)(_2hrs_avg_cav), 10);
	//	Uart_Send_String(", NA\r");
		Uart_Send_String(",PTT fault\r");
	}
}


/*****************************************************************************************************************************************
													Drift_Compensation
*Description:	1.If the averaged CAV is 2 counts higher than the current CAV value, then add 1 count to the Corrected_alarm_thr,
				2. If it??s 2 counts lower then current CAV value, then minus 1 count to the Corrected_alarm_thr,
				3. Otherwise, no change to the Corrected_alarm_thr.
				4. If the Corrected_alarm_thr reaches its limits, the chamber fault will occur
* Equation: Current_Averaged_CAV = Previous_Averaged_CAV + (Raw_CAV - Previous_Averaged_CAV) / current accumulation number.
*Arguments: None
*Return: 	None
*******************************************************************************************************************************************/
void Drift_Compensation(void)
{
	int temp_cav;
	static int drift_2_hour_counter = 0;
	if((drift_sec_counter++) >= 10)
	{
		drift_sec_counter = 0;
		drift_2_hour_counter ++;

		temp_cav = raw_cav;
		temp_cav = (temp_cav - _2hrs_avg_cav);
		temp_cav *= 100;
		temp_cav /= drift_2_hour_counter;
		temp_cav += (int)_2hrs_avg_cav * 100; 
		temp_cav /= 100;
		_2hrs_avg_cav = temp_cav;
		
		if(drift_2_hour_counter >= _2HRS_TIME_CNT)	//Check every 2 hours 720*10/3600 = 2H
		{
			//if ((eeprom._corrected_alarm_thd - _2hrs_avg_cav) > DELTA_THRESHOLD) 	/*the average CAV decrease*/
			if ((eeprom._corrected_alarm_thd - _2hrs_avg_cav) > eeprom._delta)
			{
				eeprom._corrected_alarm_thd --;
				if(eeprom._corrected_alarm_thd <= 80) 
				{
					//FLAG_DRIFT_FAULT = TRUE;
					//FLAG_PHOTO_CAL_FAILURE = TRUE;	
					FLAG_PHOTO_FAULT =  TRUE;
				}
				// eeprom._average_cav --;
				eeprom._sum -= 1;
				Write_EE_Data(LOC_CORRECTED_ALM_THRESHOLD, eeprom._corrected_alarm_thd);
				// Write_EE_Data(LOC_AVERAGE_CAV, eeprom._average_cav);

				Write_EE_Data(BACKUP_START + LOC_CORRECTED_ALM_THRESHOLD, eeprom._corrected_alarm_thd);
				// Write_EE_Data(BACKUP_START + LOC_AVERAGE_CAV, eeprom._average_cav);
			}
			//else if((eeprom._corrected_alarm_thd - _2hrs_avg_cav) < DELTA_THRESHOLD) /*the average CAV increase*/
			else if((eeprom._corrected_alarm_thd - _2hrs_avg_cav) < eeprom._delta) 
			{
				if(eeprom._corrected_alarm_thd > max_drift_compst ) //uppder limit inspection.
				{
					//if(FLAG_DRIFT_FAULT == FALSE)
					if(FLAG_PHOTO_FAULT == FALSE)
					{
						//FLAG_DRIFT_FAULT = TRUE;
						//FLAG_PHOTO_CAL_FAILURE = TRUE;
						FLAG_PHOTO_FAULT = TRUE;
						Diagnostic_Event_Update(_life_cycles, LOC_MAJOR_DFRIT_COMP_FAULT);
						// ee_data = Read_EE_Data(LOC_MAJOR_DFRIT_COMP_FAULT);
						// Write_EE_Data(LOC_MAJOR_DFRIT_COMP_FAULT, ee_data >= 255? 0 : ee_data + 1);
					}
					Uart_Send_String("Photo comp fault\r");
				}
				else
				{
					//FLAG_DRIFT_FAULT = FALSE;
					FLAG_PHOTO_FAULT = FALSE;
					eeprom._corrected_alarm_thd ++;
					// eeprom._average_cav ++;
					eeprom._sum += 1;
					Write_EE_Data(LOC_CORRECTED_ALM_THRESHOLD, eeprom._corrected_alarm_thd);
					// Write_EE_Data(LOC_AVERAGE_CAV, eeprom._average_cav);

					Write_EE_Data(BACKUP_START + LOC_CORRECTED_ALM_THRESHOLD, eeprom._corrected_alarm_thd);
					// Write_EE_Data(BACKUP_START + LOC_AVERAGE_CAV, eeprom._average_cav);
				}
			}
			Save_Checksum(eeprom._sum);
			
			pre_alarm_condition = (u8)((float)eeprom._corrected_alarm_thd * LEVEL_PRE_ALARM);
			exit_alarm_condition =  (u8)((float)eeprom._corrected_alarm_thd * LEVEL_EXIT_ALARM);

			Uart_Send_String("Comp--\0");
			// _itoa((u16)eeprom._average_cav);
			// Uart_Send_String(", ");
			_itoa((u16)_2hrs_avg_cav, 10);
			Uart_Send_String(", ");
			_itoa((u16)eeprom._corrected_alarm_thd, 10);	
			Uart_Send_Byte('\r');	
			
			drift_2_hour_counter = 0;
			_2hrs_avg_cav = raw_cav;
		}
	}
}




