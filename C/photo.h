/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#ifndef _PHOTO_H__
#define _PHOTO_H__

#include "sys.h"



#define	Current_300mA				1
#define Current_200mA				0

//Smoke detection cycle during normal sleep
#define PHOTO_SAMPLE_PERIOD		    10	// N*1S
#define HUSH_9_MIN_TIMEOUT 			(u16)540		//Hush timeout set to 9 min
//#define	C_SM_BD_ZERO_REF	    30		//unused delta threhsold
#define	DELTA_THRESHOLD				50 		// 45 Fixed alarm threshold
#define	PHOTO_CAL_UPPER_LEVEL		100		// photo ouput upper limit during calibraiton 100
#define	PHOTO_CAL_LOWER_LEVEL		30		// photo output lower limit during calbration
#define	CAL_30_AVERAGE_CAV			30		// Calibraiton sampling number
#define	CAL_15_AVERAGE_CAV			15		// Calibraiton sampling number
// #define	LOC_PHOTO_SUPV_LIMIT			30
//#define	ALM_THD_COMP_LIMIT 			220		//160 190 220  Alarm threshold upper limit, NOTE daniel gee: change 253 to 220
#define	_2HRS_TIME_CNT	 			(u16)720		// Drift compensation time counter N * 10s?20 * 10 = 2hrs
#define LEVEL_PRE_ALARM				(float)0.85		// pre-alarm limit
#define	LEVEL_EXIT_ALARM			(float)0.95   // exit alarm condition
#define LEVEL_DRIFT					(float)1.3
#define	ALARM_CONFIRM_CNT			3
// #define OPAMP_DELAY(ms)		{GCC_DELAY(ms);}	//OPA steady time 600us
#define CAL_30min_AVERAGE			1800

// DAC settings for normal and calibration
// At 25C: P_DAC = (255 * Iled / .6) - 48.66 with .5 ohm sense
// At 25C: P_DAC = (340 * Iled) - 48.66 with .4 ohm sense
#define		SMK_PHOTO_IRED_CAL_SETTING_LOW	.3			// amps
#define		SMK_PHOTO_IRED_CAL_SETTING_HIGH	.55			// amps
#define	SMK_LED_CAL_DRIVE_DELTA		(u8)((SMK_PHOTO_IRED_CAL_SETTING_HIGH - SMK_PHOTO_IRED_CAL_SETTING_LOW) * 1000 )

#define FIXED8_8CONST(A,B) (u16)(((u16)(A<<8)) +(u16)((B *(1<<8))))
#define FIXED10_6CONST(A,B) (u16)(((u16)(A<<6)) +(u16)((B *(1<<6))))

//#define SMK_ALARM_OFFSET_10_6		FIXED10_6CONST(49, .6)
//#define SMK_ALARM_SENS_SCALE_10_6	FIXED10_6CONST(152, .0)
//#define SMK_ALARM_OFFSET_SCALE_10_6	FIXED10_6CONST(1, .01)
//#define SMK_ALARM_OFFSET_10_6		FIXED10_6CONST(5, .9)//FIXED10_6CONST(5, .9)
//#define SMK_ALARM_SENS_SCALE_10_6	FIXED10_6CONST(8, .0)
//#define SMK_ALARM_OFFSET_SCALE_10_6	FIXED10_6CONST(1, .97)
#define SMK_ALARM_OFFSET_10_6		FIXED10_6CONST(4, .5)//FIXED10_6CONST(5, .9)
#define SMK_ALARM_SENS_SCALE_10_6	FIXED10_6CONST(13, .0)
#define SMK_ALARM_OFFSET_SCALE_10_6	FIXED10_6CONST(1, .93)

#define SMK_ALARM_OFFSET_FHK		78//45			//4.5
#define SMK_ALARM_SENS_SCALE_FHK	42//13
#define SMK_ALARM_OFFSET_SCALE_FHK	201//193			//1.93

//extern void	S_SM_INI(void);	
extern void AFE_Init(void);
extern void Read_Photo_Output(void);
extern void Smoke_Calibration(void);
extern void Smoke_Alarm_Check(void);
// extern u8   avg_cav;		// initial clean air value
// extern u8	photo_dark_offset;
extern u8 	ptt_raw_cav;
extern u8	raw_cav;
extern u8 	init_cav;			
extern u8	photo_cal_lower_lvl;	
extern u8	photo_cal_upper_lvl;
// extern u8	chamber_supv_limit;
//extern u8	photo_cal_sampling_cnt;
// extern u8	alarm_threshold;
extern u8   _2hrs_avg_cav;
//extern u8	R_SM_ALARM_CHECK;
// extern byte_type R_SM_LIB_STATE;    	//do not change

//#define		F_V_CONVER		R_SM_LIB_STATE.bits.bit0	
//#define		F_UL_BAT_L		R_SM_LIB_STATE.bits.bit1	

extern unsigned int checksum;

void All_OPamps_Enabled(void);			//OPA power on and enable
void All_OPamps_Disabled(void);		//OPA power off and disable
void IRED_LED_On(void);		//ISINK0 turn on, to output 200mA for IRED LED
void IRED_LED_Off(void);		//ISINK0 turn off
void Photo_Params_Init(void);	//photo related parameters
//void IRED_Standby_Set(void);
void IRED_Standby_Set(u8 Current);
void IRED_Ptt_Set(void);		// IRED config for PTT test
void Drift_Compensation(void);	
void Push_To_Test_Check(void);
//u16 SMK_Multipy_Scaled(u16 a, u16 b, u16 scale);
//void Hush_Check(void);
//void High_Temperature_Process(void);
//u8 Moving_Average_Calc(u8 raw_cav);
#endif