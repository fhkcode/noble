/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/
#include "sys.h"
#include "BA45F5350.h"
#include "adc.h"
#include "temperature.h"
#include "eeprom.h"
#include "uart.h"

/**************************************************************************************************************************
													temperature read
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
u8 Temperature_Read(void)
{
	u8 temperature_return = 0;
	
	_pbs11 = 1;
	_pbs10 = 0;
	
	temperature_return = Read_ADC_Channel(AN2,ADC_8BIT);
	
	Uart_Send_String("temperature:\0");
	_itoa((u16) temperature_return, 10);
	Uart_Send_Byte('\r');	
	
	return 	temperature_return;
}

#ifdef _CONFIGURE_TEMPERATURE_COMPENSATION
/*****************************************************************************************************************************************
													temperature compensation
*Description:	 
*Arguments: None
*Return: 	None
*******************************************************************************************************************************************/
void Temperature_Compensation(void)
{
	static u16 _1_sec_temp_com_counter = 0;
	u8 compenstation_temp = 0;
	u8 temp_cav = 0;
	
	_1_sec_temp_com_counter ++;
	if(_1_sec_temp_com_counter >= 10)
	{
		_1_sec_temp_com_counter = 0;
		temp_cav = Read_ADC_Channel(AN0, ADC_8BIT);	
		
		if(temp_cav >= COMPENSATION_THRESHOLD_LSB) 	
		{
			if(temp_cav < LOW_TEMP_L1)
			{
				compenstation_temp = LOW_TEMP_COM_L2;
			}
			else if(temp_cav < LOW_TEMP_L2)
			{
				compenstation_temp = LOW_TEMP_COM_L1;
			}
			else if((temp_cav >= NORMAL_TEMP_L3) && (temp_cav < HIGH_TEMP_L4))
			{
				compenstation_temp = HIGH_TEMP_COM_L1;
			}
			else if((temp_cav >= HIGH_TEMP_L4) && (temp_cav < COMPENSATION_THRESHOLD_MSB))
			{
				compenstation_temp = HIGH_TEMP_COM_L1;	
			}
			else
			{
				return;	
			}
		//
			if((temp_cav < LOW_TEMP_L1) || (temp_cav < LOW_TEMP_L2))
			{
				eeprom._corrected_alarm_thd = eeprom._corrected_alarm_thd + compenstation_temp;
			}
			else if((temp_cav < HIGH_TEMP_L4) || (temp_cav<COMPENSATION_THRESHOLD_MSB))
			{
				eeprom._corrected_alarm_thd = eeprom._corrected_alarm_thd - compenstation_temp;
			}
			if(eeprom._corrected_alarm_thd <= C_SM_ALARM_DATA_L) 
			{
				eeprom._corrected_alarm_thd = C_SM_ALARM_DATA_L;
			}	
		}	
	}
	
}
#endif