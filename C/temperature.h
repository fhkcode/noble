/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/ 

#ifndef _TEMPERATURE_H__
#define _TEMPERATURE_H__

#define _CONFIGURE_TEMPERATURE_COMPENSATION					1


/**************************************
temperature compensation
**************************************/	
#if	_CONFIGURE_TEMPERATURE_COMPENSATION	
	#define COMPENSATION_THRESHOLD_LSB 		2		//If the temperature ad is less than this value, 
													//no temperature compensation shall be made
	#define COMPENSATION_THRESHOLD_MSB 		254		//; If the temperature ad is greater than this value, 
													//no temperature compensation shall be made
	
	#define LOW_TEMP_L1		75		//Low temperature level 2 AD	x<L1
	#define LOW_TEMP_L2		105		//Low temperature level 1 AD	x<L2
	#define	NORMAL_TEMP_L3		145		//normal temperature AD  		L2<x<L3
	#define	HIGH_TEMP_L4		165		//high temperature level 1AD(L3<x<L4), 	X>L4�ж�Ϊ���¶���AD	
	
	#define	LOW_TEMP_COM_L2				1		//Low temperature level 2 R_SM_ALARM compensation n
	#define	LOW_TEMP_COM_L1				1		//Low temperature level 1 R_SM_ALARM compensation n
	#define	HIGH_TEMP_COM_L1			1		//high temperature level 1 R_SM_ALARM compensation
	#define HIGH_TEMP_COM_L2			1		//high temperature level 1 R_SM_ALARM compensation
	
	#define C_SM_ALARM_DATA_L	10		//After compensation, if R_ SM_ When alarm is less than this value, 
										//then R_ SM_ ALARM=C_ SM_ ALARM_ DATA_ L
#endif	

u8 Temperature_Read(void);
void Temperature_Compensation(void);

#endif