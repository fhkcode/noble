/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#include "uart.h"
#include "sys.h"
/*#include "string.h"*/


unsigned char rx_buffer[BUFFER_LIMIT];
unsigned char rx_count;

byte_type	flag_uart =  {.byte = 0};


/**************************************************************************************************************************
													Uart_Init
*Description: 	Initialize the serial port parameters.
				The Tx is using FW to simulate the hardware behaviours.
				Baud rate = 19200.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Uart_Init(void)
{
	_pac2 = 1;
	GCC_DELAY(2000);
	if(_pa2 == 1)
	{
		_papu2  = 0;
		_pac2 = 1;
		_pas05 = 1;
		_pas04 = 0;
		
		_ifs11 = 0;
		_ifs10 = 1;
		
		//_ubrg = 25;			/*Set baud rate to 8000000/(16*(25+1))=19230(19200)*/
		_brg = 25;
		_simc0 = 0b11110000;
		//_uucr1 = 0b10000000;
		_ucr1 = 0b10000000;
		//_uucr2 = 0b11100100;
		_ucr2 = 0b11100100;

		//_usime = 1;		/*UART interrupt*/
		_sime = 1;
		//_uwake = 1;		/*Enable UART wakeup function*/
		_wake = 1;
		_mfe = 1;
		_ure = 1;
		FLAG_TX_ENABLE = TRUE;
		
		_pa2 = 1;
		_papu2 = 1;
		_urf = 0;
	}
}

/**************************************************************************************************************************
													Uart_Send_Byte
*Description:	TX uses a general IO to simulate the behavior of hardware transmit function.
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Uart_Send_Byte(unsigned char byte)
{
	u8	bits = 8, i=0;
	if(FLAG_TX_ENABLE)
	{
		// Uart_Send_Bits(data);
		_emi = 0;
		// bits=8;
		P_RS_TX = 0;
		GCC_DELAY(104);
	//	GCC_DELAY(90);	//pusle withd = 108.2us
		for(i = 0; i < bits; i++)
		{
			//P_RS_TX = (byte & 0x01)? 1 : 0;
			if(byte & 0x01)
			{
				P_RS_TX = 1;	
			}
			else
			{
				P_RS_TX = 0;
			}
		//	GCC_DELAY(97);
			GCC_DELAY(91);	//pusle withd = 52us
			byte = byte >> 1;
		}
		GCC_DELAY(3);	
		P_RS_TX = 1;
		GCC_DELAY(92);
		//GCC_DELAY(80);
		_emi=1;	
	}	
}

/**************************************************************************************************************************
													Uart_Send_String
*Description:	Print the string through the serial port.
*Arguments:	*str	a pointer of the string.
*Return: 	None
***************************************************************************************************************************/
void Uart_Send_String(const char *str)
{
	if(FLAG_TX_ENABLE)
	{
		while (*str != '\0')
		{
			Uart_Send_Byte(*str);
			str++;
		}
	}
}

/**************************************************************************************************************************
													_itoa
*Description:	itoa function is to convert numbers to a ascii characters in decimal or hexadecimal.
*Arguments:	number	the value to be converted.
			radix	10 for decimal, 16 for hexdecimal.
*Return: 	None
***************************************************************************************************************************/
void _itoa(unsigned int number, unsigned char radix)
{
	if(FLAG_TX_ENABLE)
	{
		const unsigned char *ptalpha = (unsigned char *)"0123456789ABCDEF";
		unsigned char str[4];
		unsigned char i = 0;

		do 
		{
			str[i] = ptalpha[number % radix];
			number /= radix;
			i++;
		}while((number / radix !=0)||(number % radix !=0));

		if((radix == 16) && (i == 1))	/*If number is a hexadecimal, output a 0 in front of number 0~9*/
		{
			Uart_Send_Byte('0');
		}
		while(i != 0)
		{
			i--;
			Uart_Send_Byte(str[i]);
		}
	}
}



/**************************************************************************************************************************
													Uart_Receive_Process
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Uart_Receive_Process(void)
{
	unsigned char i;
	
	if(_rxif)
	{
		if(rx_count < BUFFER_LIMIT)
		{	
			rx_buffer[rx_count] = _txr_rxr;
			if(rx_buffer[rx_count] == '\r')
			{
				FLAG_COMMD_VAILD = TRUE;
			}
			rx_count ++;
			_urf = 0;
			if(_oerr || _ferr)			//Serial port overflow error, read _usr and _txr_rxr,then clear
			{
				//u8 error_buffer = 0;
				for(i = 0; i < BUFFER_LIMIT; i++)
				{
					_acc = _usr;
					_acc = 0;
					_acc = _txr_rxr;
					_acc = 0;
				}
			}
		}
	}
}

/**************************************************************************************************************************
													Uart_RX_Buffer_Clear
*Description:
*Arguments:	None
*Return: 	None
***************************************************************************************************************************/
void Uart_RX_Buffer_Clear(void)
{
	unsigned char i;

	FLAG_RX_ACTIVE = FALSE;
	FLAG_COMMD_VAILD = FALSE;
	rx_count = 0;
	for(i = 0; i < BUFFER_LIMIT; i++)
	{
		rx_buffer[i] = 0;
	}
}

/**************************************************************************************************************************
													Uart_Ascii_To_Hex
*Description:	Convert the ascii characters to the hex format
*Arguments:	data1		high byte of hex
			data2		low byte of hex
*Return: 	hex format character
***************************************************************************************************************************/
unsigned char Uart_Ascii_To_Hex(unsigned char data1,unsigned char data2)
{
	unsigned char _bytes[2] = {0}; 
	if(FLAG_TX_ENABLE){
		_bytes[1] = (data1 <= '9')? (data1 - 48) : (data1 - 55);	/* ascii 0 is number 48, ascii A is number 65*/
		_bytes[0] = (data2 <= '9')? (data2 - 48) : (data2 - 55);	
		_bytes[0] = (_bytes[1] << 4) | _bytes[0];
	}
	return 	_bytes[0];
}

