/**************************************************************************************************************************
* The contents of this file are Kidde proprietary and confidential.
***************************************************************************************************************************
* Author: Daniel Gee
* Initially created: 2021/1/8
*
**************************************************************************************************************************/

#ifndef	_UART_H__
#define _UART_H__

#include "BA45F5350.h"
#include "sys.h"


#define	BUFFER_LIMIT            10	

#define	DUBUG_TEST              1	//AA for test.
#define	CMD_CONSTANT_TONE       2	//A4 to turn on constant tone
#define	CMD_REVISE_EE           3	//c1 command to revise EE data
#define	CMD_FW_REV              4	//C0 command to print FW version
#define	CMD_GDUMP               5	//G command to dump EE data
#define	CMD_BAT_TEST            6	//b command for battery test
#define	CMD_CAL_RESET           7	// M1 restore EE data to default
#define	CMD_OPAMP_DEBUG         8	// AN turn on OPAMP 1 output to PA5
#define	CMD_FAST_MODE           9	//A1 command, fast sampling mode
#define	CMD_LITHIUM_TEST		10	//L command, lithium test
#define	CMD_SPEED_TIME			11	//S1 command, 1min to 1 hour, S2 comand, 1 min to 1 day

extern	byte_type	flag_uart;
#define	FLAG_RX_ACTIVE	    flag_uart.bits.bit0
#define	FLAG_COMMD_VAILD	flag_uart.bits.bit1
#define FLAG_TX_ENABLE      flag_uart.bits.bit2

void Uart_Init(void);
// void Uart_Send_Bits(u8 _data);
void Uart_Send_Byte(unsigned char byte);
void Uart_Receive_Process(void);
void Uart_RX_Buffer_Clear(void);
// signed char Uart_FindByte(unsigned char dat);
void Uart_Send_String(const char *str);
void _itoa(unsigned int number, unsigned char radix);
// void Uart_Send_Ascii_Hex(unsigned char data);
unsigned char Uart_Ascii_To_Hex(unsigned char data1,unsigned char data2);

extern unsigned char rx_buffer[BUFFER_LIMIT];
extern unsigned char rx_count;
// extern unsigned char R_UART_WAIT_CNT;
#endif